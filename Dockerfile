FROM python:3
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && apt-get install -y nodejs build-essential libssl-dev && \
python3 --version && \
node -v && \
npm -v
CMD ["node", "-v"]